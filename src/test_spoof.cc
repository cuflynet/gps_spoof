#include<stdio.h>
#include<signal.h>
#include<unistd.h>
#include <gps_spoof/spoofer.h>
//Test code for the spoofer object
using namespace std;

int shouldQuit = 0;

void sigint_handler(int signo)
{
  if (signo == SIGINT)
    {
      printf("received SIGINT, exiting\n");
      shouldQuit = 1;
    }
}

int main(int argc, char** argv)
{
  Spoofer localNED;
  string portName;
  int baud = 115200;

  if (argc > 2)
    portName = string(argv[1]);
  else
    portName = string("/dev/ttyVirtualS0");

  if (argc > 3)
    baud = atoi(argv[2]);
  
  signal(SIGINT, sigint_handler);

  int ret = localNED.init(portName.c_str(), baud, 10);

  if (ret == localNED.SPOOF_FAILURE)
    {
      printf("Unable to init gps spoofer on port: %s\n", portName.c_str());
      exit(1);
    }
  spoof_packet_t local;
  memset(&local, 0, sizeof(local));
  
  while (!shouldQuit)
    {
      float altitude;
      memcpy(&altitude, &local.altitude, sizeof(altitude));
      altitude += 1.0;
      memcpy(&local.altitude, &altitude, sizeof(altitude));
      local.serial += 1;
      localNED.postMessage(&local);
      
      usleep(50000);
    }
}
