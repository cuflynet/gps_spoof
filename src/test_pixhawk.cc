#include <stdlib.h>
#include <stdio.h>
#include <signal.h>
#include <unistd.h>
#include <string>
#include <iostream>

#include <serial/serial.h>
#include <gps_spoof/protocol.h>

//Test code for the pixhawk side of the local NED gps spoof protocol
using namespace std;

int shouldQuit = 0;

void sigint_handler(int signo)
{
  if (signo == SIGINT)
    {
      printf("received SIGINT, exiting\n");
      shouldQuit = 1;
    }
}

float makeFloat(uint32_t src)
{
  float output;
  memcpy(&output, &src, sizeof(src));
  return output;
}

void processPacket(spoof_packet_t *packet)
{
  printf("Serial: %d N: %1.3f E: %1.3f A: %1.3f\n", packet->serial, makeFloat(packet->northing), makeFloat(packet->easting), makeFloat(packet->altitude));
}

int main(int argc, char** argv)
{

  string portName;
  int baud = 115200;
  serial::Serial *mPort;

  if (argc > 1)
    portName = string(argv[1]);
  else
    portName = string("/dev/ttyVirtualS1");

  if (argc > 2)
    baud = atoi(argv[2]);

  cout << "Using port: " << portName << " at baud:" << baud << endl;
  try
    {
      mPort = new serial::Serial(portName, baud, serial::Timeout::simpleTimeout(1000));
    }
  catch (serial::IOException e)
    {
      cout << __FILE__ << ": Unable to open port:" << portName << endl;
      return -1;
    }
  
  signal(SIGINT, sigint_handler);


  //Send an inquiry:
  string cmd = "INQ\n";
  size_t written;

  while (!shouldQuit)
    {
      cout << "Sending INQ:" << endl;
      written = mPort->write(cmd);
      string result = mPort->readline();
      if (result.length() > 0)
	{
	  cout << "Received: " << result.c_str() << endl;
	  break;
	}
      sleep(1);
    }
  if (shouldQuit)
    return 0;

  //Start the data stream
  cmd = "ON\n";
  mPort->write(cmd);

  //Receive a packet stream
  spoof_packet_t thePacket;
  size_t bytesRead;
  while (!shouldQuit)
    {
      //Blocking read, this should be nice and quick
      bytesRead = mPort->read((uint8_t*)&thePacket, sizeof(thePacket));
      if (bytesRead != sizeof(thePacket))
	{
	  cout << "Short read, only picked up " << bytesRead << " of " << sizeof(thePacket) << " requested" << endl;
	  return -1;
	}

      processPacket(&thePacket);
    }
  
  //Stop the data stream
  cmd = "OFF\n";
  mPort->write(cmd);
    
   return 0;
  
   
}
