/* Implement a serial port handler to drive the GPS spoof protocol
   Has an inbound queue to accept messages for transmit

*/

#include <gps_spoof/spoofer.h>

Spoofer::Spoofer()
{
  streamingOn = false;
  memset((void*)&lastInfo, 0, sizeof(lastInfo));
}

Spoofer::~Spoofer()
{

  if (m_port)
    delete m_port;
}


void Spoofer::postMessage(spoof_packet_t *src)
{
  m_infoLock.lock();
  memcpy(&lastInfo, src, sizeof(lastInfo));
  m_infoLock.unlock(); 
}

void Spoofer::runProtocol()
{
  cout << "Protocol started" << endl;
  //The serial port is open and ready at this point.
  //Start a main loop that checks for commands
  string rawCmd;
  while (true)
    {
      uint8_t cmdBuf;

      if (m_port->read(&cmdBuf, 1) > 0)
	{
	  //Something received, append to the rawCmd
	  rawCmd.append(1, cmdBuf);
	}

      if ((rawCmd.length() > 0) && (rawCmd.find("\n") != string::npos))
	{
	  //try to decode:
	  //cout << "Received control sequence:" << rawCmd.c_str() << endl;
	  if (rawCmd.find("INQ",0,3) != string::npos)
	    {
	      cout << "Responding to INQ" << endl;
	      m_port->write("gps_spoof\n");
	      rawCmd.clear();
	    }
	  else if (rawCmd.find("ON",0,2) != string::npos)
	    {
	      cout << "Enabling data streaming" << endl;
	      streamingOn = true;
	      rawCmd.clear();
	    }
	  else if (rawCmd.find("OFF",0,3) != string::npos)
	    {
	      cout << "Disabling data streaming" << endl;
	      streamingOn = false;
	      rawCmd.clear();
	    }
	  else
	    {
	      cout << "Unknown protocol command: " << rawCmd << endl;
	      rawCmd.clear();
	    }
	}

      //If streaming is on, publish some data
      if (streamingOn)
	{
	  //Push the latest data
	  m_infoLock.lock();
	  m_port->write((uint8_t*)&lastInfo, sizeof(lastInfo));
	  m_infoLock.unlock();
	}
      usleep(1000);
    }
}

int Spoofer::init(const char* portName, unsigned int baud, unsigned int timeout)
{
  //Attempt to open the port
  try
    {
      m_port = new Serial(portName, baud, serial::Timeout::simpleTimeout(timeout)); 
    }
  catch (serial::IOException e)
    {
      cout << __FILE__ << ": Unable to open port:" << portName << endl;
      return SPOOF_FAILURE;
    }
  
  
  if (!m_port->isOpen())
    {
      //Unable to open serial port for some reason
      cout <<  __FILE__ << ": Unable to open port:" << portName << endl;
      return SPOOF_FAILURE;
    }
  
  //The serial protocol is initiated by the remote end
  //Start our worker thread:
  m_thread = boost::thread(&Spoofer::runProtocol, this);
  return SPOOF_SUCCESS;
}
