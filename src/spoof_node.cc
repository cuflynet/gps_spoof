/* GPS Spoofer node
   Accepts a PoseStamped message
   Translates and publishes a protocol to the serial port

*/

#include <gps_spoof/spoofer.h>
#include <ros/ros.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

#include <geometry_msgs/TransformStamped.h>

using namespace std;
//

#define DEFAULT_POSE_TOPIC "/pose"
#define DEFAULT_PORT "/dev/ttyUSB0"
#define DEFAULT_BAUD 9600

Spoofer localNED;
ros::Subscriber poseSub;

int poseCount = 0;

void print_usage(const char *prog)
{
  printf("Usage: %s\n", prog);
  printf("  pose: pose topic to connect to (default: %s)\n", DEFAULT_POSE_TOPIC);
  exit(1);
}

float makeFloat(uint32_t src)
{
  float output;
  memcpy(&output, &src, sizeof(src));
  return output;
}

uint32_t makeUint32_t(float src)
{
  uint32_t output;
  memcpy(&output, &src, sizeof(src));
  return output;
}

void poseCallback(const geometry_msgs::TransformStamped::ConstPtr& msg)
{
  spoof_packet_t local;
  memset(&local, 0, sizeof(local));
  local.northing = makeUint32_t(msg->transform.translation.x);
  local.easting = makeUint32_t(msg->transform.translation.y);
  local.altitude = makeUint32_t(msg->transform.translation.z);

  local.x = makeUint32_t(msg->transform.rotation.x);
  local.y = makeUint32_t(msg->transform.rotation.y);
  local.z = makeUint32_t(msg->transform.rotation.z);
  local.w = makeUint32_t(msg->transform.rotation.w);
  
  local.serial = ++poseCount;
  localNED.postMessage(&local);
}


int main(int argc, char ** argv)
{
  //Initialize ROS
  ros::init(argc, argv, "tf_camcal", ros::init_options::AnonymousName);
  /* check command line arguments */

  ros::NodeHandle nh("~");


  string port = DEFAULT_PORT;
  int baud = DEFAULT_BAUD;
  string sourcePose = string(DEFAULT_POSE_TOPIC);
  
  nh.getParam("port", port);
  nh.getParam("baud", baud);
  nh.getParam("pose_topic", sourcePose);
  
  ROS_INFO("Using port [%s] at baud [%d]", port.c_str(), baud);
  ROS_INFO("Using pose topic: [%s]", sourcePose.c_str());
  int ret = localNED.init(port.c_str(), baud, 10);
  if (ret == localNED.SPOOF_FAILURE)
    {
      printf("Unable to init gps spoofer on port: %s\n", port.c_str());
      exit(1);
    }
  
  poseSub = nh.subscribe(sourcePose, 1000, poseCallback);
  ros::spin(); 

  return 0;
};
