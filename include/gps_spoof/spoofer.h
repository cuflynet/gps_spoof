
#ifndef __SPOOFER_H_
#define __SPOOFER_H_

#include <boost/thread.hpp>
#include <gps_spoof/protocol.h>
#include <serial/serial.h>

using namespace serial;
using namespace std;


class Spoofer
{
 public:
  Spoofer();
 ~Spoofer();
 void postMessage(spoof_packet_t *src);
 static const int SPOOF_FAILURE = 0;
 static const int SPOOF_SUCCESS = 1;
 int init(const char* portName, unsigned int baud, unsigned int timeout);
 
 private:
 bool streamingOn;
 void runProtocol();
 spoof_packet_t lastInfo;
 Serial *m_port;
 boost::thread m_thread;
 boost::mutex m_infoLock;
};

#endif
