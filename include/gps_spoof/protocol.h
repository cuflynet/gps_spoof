#ifndef __PROTOCOL_H_
#define __PROTOCOL_H_

/* Protocol for the GPS spoofed UTM coordinates for visual or Vicon coordinate systems */
#include <stdint.h>

typedef struct __attribute__((packed))
{
  uint32_t serial; 
  uint32_t northing; // float32
  uint32_t easting;  // float32
  uint32_t altitude; // float32

  //Quaternion for the pixhawk
  uint32_t x;
  uint32_t y;
  uint32_t z;
  uint32_t w;
} spoof_packet_t;

/* Serial protocol:
   
Initially, the port is dead. There are several strings that control the data stream. Inbound is data from the port to the spoof daemon. 

Purpose: Identify the attached GPS device
Inbound: INQ\n
Outbound: gps_spoof\n

Purpose: Start the data stream
Inbound: ON\n
Outbound: Sequence of spoof_packet_t, with sequentially increasing serial fields

Purpose: Stop the data stream
Inbound: OFF\n
Outbound: OK\n

 */

#endif
